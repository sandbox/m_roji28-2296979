INTRODUCTION
------------
Modified link in menu.
Give you menu item without link.
Replace anchor tag (<a>) with <span> or remove attribute href in <a> then
replace with #. It useful in dropdown menu style.

example:
<ul class="menu">
  <!-- standard link -->
  <li><a href="/">Home</a></li>
  <!-- white link with option ... -->
  <li><a href="#">My Account</a></li>
    <ul>
	  <!-- white link with option ... -->
      <li><a href="/user">Profile</a></li>
      <li><a href="/user/edit">Edit</a></li>
      <li><a href="/user/logout">Logout</a></li>
      <li><span>Last Login 6 hours ago</span></li> <!-- empty_link checked-->
    </ul>
</ul>

Install
-------
    1. Put this module in sites/*/modules,
    2. Enable this module then visit admin/structure/menu/manage/navigation/add
    3. you will see checkbox "Empty Link"
    4. checked and then it will remove attribute href
