<?php

/**
 * @file
 * Admin page.
 */

/**
 * Return form array.
 */
function white_link_configuration_form($form, &$form_state) {
  $form['white_link_method'] = array(
    '#title' => 'Method to build white link.',
    '#type' => 'radios',
    '#default_value' => variable_get('white_link_method', 'alt'),
    '#options' => array(
      'alt' => t('Alter theme_link() function'),
      'js' => t('Use javascript'),
    ),
    'alt' => array(
      '#description' => t("Recommended but don't use this option if your theme has implemented theme_link in template.php (e.g bartik_link)."),
    ),
    'js' => array(
      '#description' => t("Build white link with javascript, it dosn't work if client's javascript disabled."),
    ),
  );
  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  return $form;
}

/**
 * Handle submit.
 */
function white_link_configuration_form_submit($form, &$form_state) {
  // If changed, we need rebuild theme registry.
  if ($form['white_link_method']['#default_value'] != $form['white_link_method']['#value']) {
    // Clear.
    drupal_theme_rebuild();
    // Build again.
    drupal_theme_initialize();
    variable_set('white_link_method', $form_state['values']['white_link_method']);
    // Save.
    drupal_set_message(t('Theme registry rebuild.'));
  }
}
