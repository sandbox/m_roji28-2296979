/**
 * @file
 * Javascript method to build white link.
 */

(function($) {

Drupal.behaviors.whiteLink = {
  attach: function (context, settings) {
    $('.white-link', context).once('white-link', function () {
      var $this = $(this);
      var white_link = $this.attr('white_link');
      var _text = $this.text();
      switch (white_link) {
        case 'remove_link':
          var $tag = '';
          var $href = '';
          break;

        case 'span':
          var $tag = 'span';
          var $href = '';
          break;

        case 'fragment':
          var $tag = 'a';
          var $href = '#';
          break;
      }
      // End switch.
      if ($tag == '') {
        $this.replaceWith(_text);
      }
      else{
        var _new = $('<' + $tag + '></' + $tag + '>');
        _new.text(_text);
        var _attributes = this.attributes;
        for (var base in _attributes) {
          var x = parseInt(base);
          if (isNaN(x) == false) {
            var a = _attributes[base];
            if (a.name == 'white_link') {
              a.value = '';
            }
            if (a.name == 'href') {
              a.value = $href;
            }
            if (a.name != '' && a.value != '') {
              _new.attr(a.name, a.value);
            }
          }
        }
        if (white_link == 'javascript') {
          _new.attr('onclick', 'javascript:return false;');
        }
        $this.replaceWith(_new);
      }
      // End else.
    });
  }
};

})(jQuery);
